import { Document } from 'mongoose';

export interface Student extends Document {
  readonly fullName: string;
  readonly age: number;
  readonly educationStage: string;
}
