import { ObjectType, Field, Int, ID } from '@nestjs/graphql';

@ObjectType()
export class StudentType {
  @Field(() => ID)
  readonly id?: string;
  @Field()
  readonly fullName: string;
  @Field(() => Int)
  readonly age: number;
  @Field()
  readonly educationStage: string;
}
