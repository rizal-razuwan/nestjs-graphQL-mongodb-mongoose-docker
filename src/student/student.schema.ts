import { MongooseModule, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Mongoose } from 'mongoose';
import * as mongoose from 'mongoose';
@Schema()
export class Student {
  @Prop()
  fullName: string;

  @Prop()
  age: number;

  @Prop()
  educationStage: string;
}

// Mongoose.Model(Student)
export const StudentSchema = SchemaFactory.createForClass(Student);
