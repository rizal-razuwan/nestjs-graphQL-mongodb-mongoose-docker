import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { StudentType } from './dto/student.dto';
import { StudentInput } from './student.input';
import { Student } from './interfaces/student.interface';

import { Model } from 'mongoose';

@Injectable()
export class StudentService {
  constructor(@InjectModel('Student') private studentModel: Model<Student>) {}

  async create(createStudentDto: StudentInput): Promise<StudentType> {
    const createdStudent = new this.studentModel(createStudentDto);
    return await createdStudent.save();
  }

  async findAll(): Promise<StudentType[]> {
    return await this.studentModel.find().exec();
  }

//   async findOne(id: string): Promise<StudentType> {
//     return await this.studentModel.findOne({ _id: id });
//   }

  async delete(id: string): Promise<StudentType> {
    return await this.studentModel.findByIdAndRemove(id);
  }

  async update(id: string, student: Student): Promise<StudentType> {
    return await this.studentModel.findByIdAndUpdate(id, student, {
      new: true,
    });
  }
}
