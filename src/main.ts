// require('dotenv').config();
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

// const corsWhitelist = process.env.CORS_WHITELIST.split(',');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: 'http://localhost:3000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });
  await app.listen(5000);
}
bootstrap();
