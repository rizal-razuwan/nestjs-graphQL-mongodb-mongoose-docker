import { Args, Resolver, Mutation, Query } from '@nestjs/graphql';
import { StudentType } from './dto/student.dto';
import { StudentInput } from './student.input';
import { StudentService } from './student.service';
import { Student } from './interfaces/student.interface';

@Resolver(of => StudentType)
export class StudentResolver {
  constructor(private readonly studentsService: StudentService) {}

  @Query(returns => StudentType)
  async students(): Promise<StudentType[]> {
    return this.studentsService.findAll();
  }

  @Mutation(returns => StudentType)
  async createStudent(
    @Args('input') input: StudentInput,
  ): Promise<StudentType> {
    return this.studentsService.create(input);
  }

//   @Mutation(returns => StudentType)
//   async updateStudent(
//     @Args('id') id: string,
//     @Args('input') input: StudentInput,
//   ) {
//     return this.studentsService.update(id, input as Student);
//   }

  @Mutation((returns) => StudentType)
  async deleteStudent(@Args('id') id: string) {
    return this.studentsService.delete(id);
  }
}
