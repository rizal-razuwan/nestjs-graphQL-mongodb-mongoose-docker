import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';

import { CatModule } from './cat/cat.module';
import { StudentModule } from './student/student.module';

// const url = process.env.MONGO_URL || 'localhost';
// const corsWhitelist = process.env.CORS_WHITELIST.split(',');
@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/maibelajaq-local'),
    // MongooseModule.forRoot(process.env.DB_URL, { useNewUrlParser: true }),
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      playground: true,
      debug: true,
      context: ({ req, res }) => ({ req, res }),
      cors: {
        credentials: true,
        origin: 'http://localhost:3000',
      },
    }),
    CatModule,
    StudentModule,
  ],
})
export class AppModule {}
