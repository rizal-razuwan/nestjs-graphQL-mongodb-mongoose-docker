FROM node:lts-alpine
WORKDIR /app
COPY  ${PWD}/package.json ./ 
COPY yarn.lock /app
RUN yarn install 
# COPY . .
# RUN yarn compodoc
COPY . .
RUN yarn build
EXPOSE 5000
EXPOSE 8080
CMD ["yarn", "start:prod"]