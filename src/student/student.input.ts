import { InputType, Field, Int } from '@nestjs/graphql';

@InputType()
export class StudentInput {
  @Field()
  readonly fullName: string;
  @Field(() => Int)
  readonly age: number;
  @Field()
  readonly educationStage: string;
}
